package cs105;

public class Circle {
    public double x;
    public double y;
    public double radius;

    public double getX() {
        return x;
    }

    public void setX(double X) {
        x = X;
    }

    public double getY() {
        return y;
    }

    public void setY(double Y) {
        y = Y;
    }

    public double getR() {
        return radius;
    }

    public void setR(double r) {
        radius = r;
    }

    public double getArea() {
        return 3.14 * radius * radius;
    }

    public void translate(double dx, double dy) {
        x += dx;
        y += dy;
    }

    public double scale(double scaler) {
        radius *= scaler;
        return radius;
    }
}
